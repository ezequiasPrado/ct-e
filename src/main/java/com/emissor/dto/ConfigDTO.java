package com.emissor.dto;

public class ConfigDTO {

    private String certificado;
    private String senha;
    private String schemasNFe;
    private String schemasCTe;

    public ConfigDTO() {
    }

    public String getCertificado() {
        return certificado;
    }

    public void setCertificado(String certificado) {
        this.certificado = certificado;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSchemasNFe() {
        return schemasNFe;
    }

    public void setSchemasNFe(String schemasNFe) {
        this.schemasNFe = schemasNFe;
    }

    public String getSchemasCTe() {
        return schemasCTe;
    }

    public void setSchemasCTe(String schemasCTe) {
        this.schemasCTe = schemasCTe;
    }
}
