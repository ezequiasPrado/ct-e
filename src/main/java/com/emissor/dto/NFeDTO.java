package com.emissor.dto;

import java.io.Serializable;

public class NFeDTO implements Serializable {

    private String cnpj;
    private String modelo;
    private String tipoEmissao;
    private String cfop;
    private String tpImp;
    private String natOp;
    private int numeroNfe;
    private int serie;
    private String tpNF;
    private String idDest;
    private String codMunFG;
    private String finNFe;
    private String indFinal;
    private String indPres;
    private String procEmi;
    private String emitCnpj;
    private String emitInscricaoEstadual;
    private String emitXNome;
    private String crt;
    private String emitFone;
    private String emitCep;
    private String emitUF;
    private String emitXLgr;
    private String emitNro;
    private String emitXCpl;
    private String emitBairro;
    private String emitCMun;
    private String emitXMun;

    private String destCnpj;
    private String indIEDest;
    private String destXNome;
    private String destEmail;
    private String destFone;
    private String destCep;
    private String destUF;
    private String destXLgr;
    private String destNro;
    private String destXCpl;
    private String destBairro;
    private String destCMun;
    private String destXMun;
    private String destCPais;
    private String destXPais;
    private ConfigDTO config;

    public NFeDTO() {
    }

    public ConfigDTO getConfig() {
        return config;
    }

    public void setConfig(ConfigDTO config) {
        this.config = config;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipoEmissao() {
        return tipoEmissao;
    }

    public void setTipoEmissao(String tipoEmissao) {
        this.tipoEmissao = tipoEmissao;
    }

    public String getCfop() {
        return cfop;
    }

    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    public String getTpImp() {
        return tpImp;
    }

    public void setTpImp(String tpImp) {
        this.tpImp = tpImp;
    }

    public String getNatOp() {
        return natOp;
    }

    public void setNatOp(String natOp) {
        this.natOp = natOp;
    }

    public int getNumeroNfe() {
        return numeroNfe;
    }

    public void setNumeroNfe(int numeroNfe) {
        this.numeroNfe = numeroNfe;
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public String getTpNF() {
        return tpNF;
    }

    public void setTpNF(String tpNF) {
        this.tpNF = tpNF;
    }

    public String getIdDest() {
        return idDest;
    }

    public void setIdDest(String idDest) {
        this.idDest = idDest;
    }

    public String getCodMunFG() {
        return codMunFG;
    }

    public void setCodMunFG(String codMunFG) {
        this.codMunFG = codMunFG;
    }

    public String getFinNFe() {
        return finNFe;
    }

    public void setFinNFe(String finNFe) {
        this.finNFe = finNFe;
    }

    public String getIndFinal() {
        return indFinal;
    }

    public void setIndFinal(String indFinal) {
        this.indFinal = indFinal;
    }

    public String getIndPres() {
        return indPres;
    }

    public void setIndPres(String indPres) {
        this.indPres = indPres;
    }

    public String getProcEmi() {
        return procEmi;
    }

    public void setProcEmi(String procEmi) {
        this.procEmi = procEmi;
    }

    public String getEmitCnpj() {
        return emitCnpj;
    }

    public void setEmitCnpj(String emitCnpj) {
        this.emitCnpj = emitCnpj;
    }

    public String getEmitInscricaoEstadual() {
        return emitInscricaoEstadual;
    }

    public void setEmitInscricaoEstadual(String emitInscricaoEstadual) {
        this.emitInscricaoEstadual = emitInscricaoEstadual;
    }

    public String getEmitXNome() {
        return emitXNome;
    }

    public void setEmitXNome(String emitXNome) {
        this.emitXNome = emitXNome;
    }

    public String getCrt() {
        return crt;
    }

    public void setCrt(String crt) {
        this.crt = crt;
    }

    public String getEmitFone() {
        return emitFone;
    }

    public void setEmitFone(String emitFone) {
        this.emitFone = emitFone;
    }

    public String getEmitCep() {
        return emitCep;
    }

    public void setEmitCep(String emitCep) {
        this.emitCep = emitCep;
    }

    public String getEmitUF() {
        return emitUF;
    }

    public void setEmitUF(String emitUF) {
        this.emitUF = emitUF;
    }

    public String getEmitXLgr() {
        return emitXLgr;
    }

    public void setEmitXLgr(String emitXLgr) {
        this.emitXLgr = emitXLgr;
    }

    public String getEmitNro() {
        return emitNro;
    }

    public void setEmitNro(String emitNro) {
        this.emitNro = emitNro;
    }

    public String getEmitXCpl() {
        return emitXCpl;
    }

    public void setEmitXCpl(String emitXCpl) {
        this.emitXCpl = emitXCpl;
    }

    public String getEmitBairro() {
        return emitBairro;
    }

    public void setEmitBairro(String emitBairro) {
        this.emitBairro = emitBairro;
    }

    public String getEmitCMun() {
        return emitCMun;
    }

    public void setEmitCMun(String emitCMun) {
        this.emitCMun = emitCMun;
    }

    public String getEmitXMun() {
        return emitXMun;
    }

    public void setEmitXMun(String emitXMun) {
        this.emitXMun = emitXMun;
    }

    public String getDestCnpj() {
        return destCnpj;
    }

    public void setDestCnpj(String destCnpj) {
        this.destCnpj = destCnpj;
    }

    public String getIndIEDest() {
        return indIEDest;
    }

    public void setIndIEDest(String indIEDest) {
        this.indIEDest = indIEDest;
    }

    public String getDestXNome() {
        return destXNome;
    }

    public void setDestXNome(String destXNome) {
        this.destXNome = destXNome;
    }

    public String getDestEmail() {
        return destEmail;
    }

    public void setDestEmail(String destEmail) {
        this.destEmail = destEmail;
    }

    public String getDestFone() {
        return destFone;
    }

    public void setDestFone(String destFone) {
        this.destFone = destFone;
    }

    public String getDestCep() {
        return destCep;
    }

    public void setDestCep(String destCep) {
        this.destCep = destCep;
    }

    public String getDestUF() {
        return destUF;
    }

    public void setDestUF(String destUF) {
        this.destUF = destUF;
    }

    public String getDestXLgr() {
        return destXLgr;
    }

    public void setDestXLgr(String destXLgr) {
        this.destXLgr = destXLgr;
    }

    public String getDestNro() {
        return destNro;
    }

    public void setDestNro(String destNro) {
        this.destNro = destNro;
    }

    public String getDestXCpl() {
        return destXCpl;
    }

    public void setDestXCpl(String destXCpl) {
        this.destXCpl = destXCpl;
    }

    public String getDestBairro() {
        return destBairro;
    }

    public void setDestBairro(String destBairro) {
        this.destBairro = destBairro;
    }

    public String getDestCMun() {
        return destCMun;
    }

    public void setDestCMun(String destCMun) {
        this.destCMun = destCMun;
    }

    public String getDestXMun() {
        return destXMun;
    }

    public void setDestXMun(String destXMun) {
        this.destXMun = destXMun;
    }

    public String getDestCPais() {
        return destCPais;
    }

    public void setDestCPais(String destCPais) {
        this.destCPais = destCPais;
    }

    public String getDestXPais() {
        return destXPais;
    }

    public void setDestXPais(String destXPais) {
        this.destXPais = destXPais;
    }
}
