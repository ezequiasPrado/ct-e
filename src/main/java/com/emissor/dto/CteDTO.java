package com.emissor.dto;

import java.io.Serializable;

public class CteDTO implements Serializable {

    private String cnpj;
    private String modelo;
    private String tipoEmissao;
    private String cfop;
    private String tpImp;
    private String natOp;
    private String numero;
    private String serie;
    private String tpCTe;
    private String xMunEnv;
    private String modal;
    private String tpServ;
    private String cMunEnv;
    private String ufEnv;
    private String retira;
    private String indIEToma;
    private String cMunIni;
    private String ufIni;
    private String xMunIni;
    private String xMunFim;
    private String cMunFim;
    private String ufFim;
    private String toma;
    private String xObs;
    private String emitCnpj;
    private String emitInscricaoEstadual;
    private String emitXNome;
    private String crt;
    private String emitFone;
    private String emitCep;
    private String emitUF;
    private String emitXLgr;
    private String emitNro;
    private String emitXCpl;
    private String emitBairro;
    private String emitCMun;
    private String emitXMun;
    private String remtCnpj;
    private String remInscricaoEstadual;
    private String remXNome;
    private String remEmail;
    private String remFone;
    private String remCep;
    private String remUF;
    private String remXLgr;
    private String remNro;
    private String remXCpl;
    private String remBairro;
    private String remCMun;
    private String remXMun;
    private String remCPais;
    private String remXPais;
    private String destCnpj;
    private String destCnpjInscricaoEstadual;
    private String destXNome;
    private String destEmail;
    private String destFone;
    private String destCep;
    private String destUF;
    private String destXLgr;
    private String destNro;
    private String destXCpl;
    private String destBairro;
    private String destCMun;
    private String destXMun;
    private String destCPais;
    private String destXPais;
    private String vTPrest;
    private String vRec;
    private String vComp;
    private String prestXNome;
    private String cst;
    private String vCarga;
    private String proPred;
    private String cUnid;
    private String tpMed;
    private String qCarga;
    private String chave;
    private String RNTRC;
    private ConfigDTO config;

    public CteDTO() {
    }

    public ConfigDTO getConfig() {
        return config;
    }

    public void setConfig(ConfigDTO config) {
        this.config = config;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipoEmissao() {
        return tipoEmissao;
    }

    public void setTipoEmissao(String tipoEmissao) {
        this.tipoEmissao = tipoEmissao;
    }

    public String getCfop() {
        return cfop;
    }

    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    public String getTpImp() {
        return tpImp;
    }

    public void setTpImp(String tpImp) {
        this.tpImp = tpImp;
    }

    public String getNatOp() {
        return natOp;
    }

    public void setNatOp(String natOp) {
        this.natOp = natOp;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getTpCTe() {
        return tpCTe;
    }

    public void setTpCTe(String tpCTe) {
        this.tpCTe = tpCTe;
    }

    public String getxMunEnv() {
        return xMunEnv;
    }

    public void setxMunEnv(String xMunEnv) {
        this.xMunEnv = xMunEnv;
    }

    public String getModal() {
        return modal;
    }

    public void setModal(String modal) {
        this.modal = modal;
    }

    public String getTpServ() {
        return tpServ;
    }

    public void setTpServ(String tpServ) {
        this.tpServ = tpServ;
    }

    public String getcMunEnv() {
        return cMunEnv;
    }

    public void setcMunEnv(String cMunEnv) {
        this.cMunEnv = cMunEnv;
    }

    public String getUfEnv() {
        return ufEnv;
    }

    public void setUfEnv(String ufEnv) {
        this.ufEnv = ufEnv;
    }

    public String getRetira() {
        return retira;
    }

    public void setRetira(String retira) {
        this.retira = retira;
    }

    public String getIndIEToma() {
        return indIEToma;
    }

    public void setIndIEToma(String indIEToma) {
        this.indIEToma = indIEToma;
    }

    public String getcMunIni() {
        return cMunIni;
    }

    public void setcMunIni(String cMunIni) {
        this.cMunIni = cMunIni;
    }

    public String getUfIni() {
        return ufIni;
    }

    public void setUfIni(String ufIni) {
        this.ufIni = ufIni;
    }

    public String getxMunIni() {
        return xMunIni;
    }

    public void setxMunIni(String xMunIni) {
        this.xMunIni = xMunIni;
    }

    public String getxMunFim() {
        return xMunFim;
    }

    public void setxMunFim(String xMunFim) {
        this.xMunFim = xMunFim;
    }

    public String getcMunFim() {
        return cMunFim;
    }

    public void setcMunFim(String cMunFim) {
        this.cMunFim = cMunFim;
    }

    public String getUfFim() {
        return ufFim;
    }

    public void setUfFim(String ufFim) {
        this.ufFim = ufFim;
    }

    public String getToma() {
        return toma;
    }

    public void setToma(String toma) {
        this.toma = toma;
    }

    public String getxObs() {
        return xObs;
    }

    public void setxObs(String xObs) {
        this.xObs = xObs;
    }

    public String getEmitCnpj() {
        return emitCnpj;
    }

    public void setEmitCnpj(String emitCnpj) {
        this.emitCnpj = emitCnpj;
    }

    public String getEmitInscricaoEstadual() {
        return emitInscricaoEstadual;
    }

    public void setEmitInscricaoEstadual(String emitInscricaoEstadual) {
        this.emitInscricaoEstadual = emitInscricaoEstadual;
    }

    public String getEmitXNome() {
        return emitXNome;
    }

    public void setEmitXNome(String emitXNome) {
        this.emitXNome = emitXNome;
    }

    public String getCrt() {
        return crt;
    }

    public void setCrt(String crt) {
        this.crt = crt;
    }

    public String getEmitFone() {
        return emitFone;
    }

    public void setEmitFone(String emitFone) {
        this.emitFone = emitFone;
    }

    public String getEmitCep() {
        return emitCep;
    }

    public void setEmitCep(String emitCep) {
        this.emitCep = emitCep;
    }

    public String getEmitUF() {
        return emitUF;
    }

    public void setEmitUF(String emitUF) {
        this.emitUF = emitUF;
    }

    public String getEmitXLgr() {
        return emitXLgr;
    }

    public void setEmitXLgr(String emitXLgr) {
        this.emitXLgr = emitXLgr;
    }

    public String getEmitNro() {
        return emitNro;
    }

    public void setEmitNro(String emitNro) {
        this.emitNro = emitNro;
    }

    public String getEmitXCpl() {
        return emitXCpl;
    }

    public void setEmitXCpl(String emitXCpl) {
        this.emitXCpl = emitXCpl;
    }

    public String getEmitBairro() {
        return emitBairro;
    }

    public void setEmitBairro(String emitBairro) {
        this.emitBairro = emitBairro;
    }

    public String getEmitCMun() {
        return emitCMun;
    }

    public void setEmitCMun(String emitCMun) {
        this.emitCMun = emitCMun;
    }

    public String getEmitXMun() {
        return emitXMun;
    }

    public void setEmitXMun(String emitXMun) {
        this.emitXMun = emitXMun;
    }

    public String getRemtCnpj() {
        return remtCnpj;
    }

    public void setRemtCnpj(String remtCnpj) {
        this.remtCnpj = remtCnpj;
    }

    public String getRemInscricaoEstadual() {
        return remInscricaoEstadual;
    }

    public void setRemInscricaoEstadual(String remInscricaoEstadual) {
        this.remInscricaoEstadual = remInscricaoEstadual;
    }

    public String getRemXNome() {
        return remXNome;
    }

    public void setRemXNome(String remXNome) {
        this.remXNome = remXNome;
    }

    public String getRemEmail() {
        return remEmail;
    }

    public void setRemEmail(String remEmail) {
        this.remEmail = remEmail;
    }

    public String getRemFone() {
        return remFone;
    }

    public void setRemFone(String remFone) {
        this.remFone = remFone;
    }

    public String getRemCep() {
        return remCep;
    }

    public void setRemCep(String remCep) {
        this.remCep = remCep;
    }

    public String getRemUF() {
        return remUF;
    }

    public void setRemUF(String remUF) {
        this.remUF = remUF;
    }

    public String getRemXLgr() {
        return remXLgr;
    }

    public void setRemXLgr(String remXLgr) {
        this.remXLgr = remXLgr;
    }

    public String getRemNro() {
        return remNro;
    }

    public void setRemNro(String remNro) {
        this.remNro = remNro;
    }

    public String getRemXCpl() {
        return remXCpl;
    }

    public void setRemXCpl(String remXCpl) {
        this.remXCpl = remXCpl;
    }

    public String getRemBairro() {
        return remBairro;
    }

    public void setRemBairro(String remBairro) {
        this.remBairro = remBairro;
    }

    public String getRemCMun() {
        return remCMun;
    }

    public void setRemCMun(String remCMun) {
        this.remCMun = remCMun;
    }

    public String getRemXMun() {
        return remXMun;
    }

    public void setRemXMun(String remXMun) {
        this.remXMun = remXMun;
    }

    public String getRemCPais() {
        return remCPais;
    }

    public void setRemCPais(String remCPais) {
        this.remCPais = remCPais;
    }

    public String getRemXPais() {
        return remXPais;
    }

    public void setRemXPais(String remXPais) {
        this.remXPais = remXPais;
    }

    public String getDestCnpj() {
        return destCnpj;
    }

    public void setDestCnpj(String destCnpj) {
        this.destCnpj = destCnpj;
    }

    public String getDestCnpjInscricaoEstadual() {
        return destCnpjInscricaoEstadual;
    }

    public void setDestCnpjInscricaoEstadual(String destCnpjInscricaoEstadual) {
        this.destCnpjInscricaoEstadual = destCnpjInscricaoEstadual;
    }

    public String getDestXNome() {
        return destXNome;
    }

    public void setDestXNome(String destXNome) {
        this.destXNome = destXNome;
    }

    public String getDestEmail() {
        return destEmail;
    }

    public void setDestEmail(String destEmail) {
        this.destEmail = destEmail;
    }

    public String getDestFone() {
        return destFone;
    }

    public void setDestFone(String destFone) {
        this.destFone = destFone;
    }

    public String getDestCep() {
        return destCep;
    }

    public void setDestCep(String destCep) {
        this.destCep = destCep;
    }

    public String getDestUF() {
        return destUF;
    }

    public void setDestUF(String destUF) {
        this.destUF = destUF;
    }

    public String getDestXLgr() {
        return destXLgr;
    }

    public void setDestXLgr(String destXLgr) {
        this.destXLgr = destXLgr;
    }

    public String getDestNro() {
        return destNro;
    }

    public void setDestNro(String destNro) {
        this.destNro = destNro;
    }

    public String getDestXCpl() {
        return destXCpl;
    }

    public void setDestXCpl(String destXCpl) {
        this.destXCpl = destXCpl;
    }

    public String getDestBairro() {
        return destBairro;
    }

    public void setDestBairro(String destBairro) {
        this.destBairro = destBairro;
    }

    public String getDestCMun() {
        return destCMun;
    }

    public void setDestCMun(String destCMun) {
        this.destCMun = destCMun;
    }

    public String getDestXMun() {
        return destXMun;
    }

    public void setDestXMun(String destXMun) {
        this.destXMun = destXMun;
    }

    public String getDestCPais() {
        return destCPais;
    }

    public void setDestCPais(String destCPais) {
        this.destCPais = destCPais;
    }

    public String getDestXPais() {
        return destXPais;
    }

    public void setDestXPais(String destXPais) {
        this.destXPais = destXPais;
    }

    public String getvTPrest() {
        return vTPrest;
    }

    public void setvTPrest(String vTPrest) {
        this.vTPrest = vTPrest;
    }

    public String getvRec() {
        return vRec;
    }

    public void setvRec(String vRec) {
        this.vRec = vRec;
    }

    public String getvComp() {
        return vComp;
    }

    public void setvComp(String vComp) {
        this.vComp = vComp;
    }

    public String getPrestXNome() {
        return prestXNome;
    }

    public void setPrestXNome(String prestXNome) {
        this.prestXNome = prestXNome;
    }

    public String getCst() {
        return cst;
    }

    public void setCst(String cst) {
        this.cst = cst;
    }

    public String getvCarga() {
        return vCarga;
    }

    public void setvCarga(String vCarga) {
        this.vCarga = vCarga;
    }

    public String getProPred() {
        return proPred;
    }

    public void setProPred(String proPred) {
        this.proPred = proPred;
    }

    public String getcUnid() {
        return cUnid;
    }

    public void setcUnid(String cUnid) {
        this.cUnid = cUnid;
    }

    public String getTpMed() {
        return tpMed;
    }

    public void setTpMed(String tpMed) {
        this.tpMed = tpMed;
    }

    public String getqCarga() {
        return qCarga;
    }

    public void setqCarga(String qCarga) {
        this.qCarga = qCarga;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getRNTRC() {
        return RNTRC;
    }

    public void setRNTRC(String RNTRC) {
        this.RNTRC = RNTRC;
    }
}
