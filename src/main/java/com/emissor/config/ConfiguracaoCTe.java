package com.emissor.config;

import br.com.swconsultoria.certificado.Certificado;
import br.com.swconsultoria.certificado.CertificadoService;
import br.com.swconsultoria.certificado.exception.CertificadoException;
import br.com.swconsultoria.cte.dom.ConfiguracoesCte;
import br.com.swconsultoria.cte.dom.enuns.AmbienteEnum;
import br.com.swconsultoria.cte.dom.enuns.EstadosEnum;
import br.com.swconsultoria.cte.exception.CteException;
import com.emissor.dto.ConfigDTO;
import com.emissor.dto.CteDTO;

import java.io.FileNotFoundException;

public class ConfiguracaoCTe {

    public static ConfiguracoesCte iniciaConfiguracoes(ConfigDTO dto) throws CteException, CertificadoException, FileNotFoundException {
        // Certificado Arquivo, Parametros: -Caminho Certificado, - Senha
        Certificado certificado = CertificadoService.certificadoPfx(dto.getCertificado(), dto.getSenha());

        return ConfiguracoesCte.criarConfiguracoes(EstadosEnum.SC, AmbienteEnum.HOMOLOGACAO,certificado, dto.getSchemasCTe());
    }
}
