package com.emissor.config;

import br.com.swconsultoria.certificado.Certificado;
import br.com.swconsultoria.certificado.CertificadoService;
import br.com.swconsultoria.certificado.exception.CertificadoException;
import br.com.swconsultoria.nfe.dom.ConfiguracoesNfe;
import br.com.swconsultoria.nfe.dom.Proxy;
import br.com.swconsultoria.nfe.dom.enuns.AmbienteEnum;
import br.com.swconsultoria.nfe.dom.enuns.EstadosEnum;
import com.emissor.dto.ConfigDTO;

import java.io.FileNotFoundException;

public class ConfiguracaoNFe {

    public static ConfiguracoesNfe iniciaConfiguracoes(ConfigDTO dto) throws CertificadoException, FileNotFoundException {
        Certificado certificado = CertificadoService.certificadoPfx(dto.getCertificado(), dto.getSenha());

        ConfiguracoesNfe config = ConfiguracoesNfe.criarConfiguracoes(EstadosEnum.SC, AmbienteEnum.HOMOLOGACAO, certificado,  dto.getSchemasNFe());

        String ip = "192.168.0.1";
        int porta = 3128;
        String usuario = "";
        String senha = "";

        config.setProxy(new Proxy(ip, porta, usuario , senha));

        return config;
    }
}
