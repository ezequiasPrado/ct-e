package com.emissor.controllers;

import br.com.swconsultoria.certificado.exception.CertificadoException;
import br.com.swconsultoria.cte.dom.ConfiguracoesCte;
import br.com.swconsultoria.cte.dom.enuns.ConsultaDFeEnum;
import br.com.swconsultoria.cte.exception.CteException;
import br.com.swconsultoria.cte.schema_100.retdistdfeint.RetDistDFeInt;
import com.emissor.config.ConfiguracaoCTe;
import com.emissor.dto.CancelamentoCTeDTO;
import com.emissor.dto.ConfigDTO;
import com.emissor.dto.CteDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.swconsultoria.cte.Cte;
import br.com.swconsultoria.cte.schema_400.retConsStatServCTe.TRetConsStatServ;

import br.com.swconsultoria.cte.dom.enuns.StatusCteEnum;
import br.com.swconsultoria.cte.schema_400.cte.TRetCTe;
import br.com.swconsultoria.cte.schema_400.cte.TUFSemEX;
import br.com.swconsultoria.cte.schema_400.cte.TUf;
import br.com.swconsultoria.cte.schema_400.cteModalRodoviario.Rodo;
import br.com.swconsultoria.cte.schema_400.cte.TCTe;
import br.com.swconsultoria.cte.util.ChaveUtil;
import br.com.swconsultoria.cte.util.ConstantesCte;
import br.com.swconsultoria.cte.util.ObjetoCTeUtil;
import br.com.swconsultoria.cte.util.XmlCteUtil;

import br.com.swconsultoria.cte.schema_400.evCancCTe.EvCancCTe;
import br.com.swconsultoria.cte.schema_400.evCancCTe.TProcEvento;
import br.com.swconsultoria.cte.schema_400.evCancCTe.TRetEvento;
import br.com.swconsultoria.cte.schema_400.evCancCTe.TEvento;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/api/cte")
public class EmissorCteController {

	@RequestMapping(value="/consultarstatus",method= RequestMethod.POST)
	public ResponseEntity<String> consultarstatus(ConfigDTO dto) throws FileNotFoundException {
		String msg = "";
		try {
			ConfiguracoesCte config = ConfiguracaoCTe.iniciaConfiguracoes(dto);

			String cnpj = ""; // TODO Preencher Cnpj
			String nsu = "000000000000000"; // TODO Preencher Nsu

			RetDistDFeInt retorno = Cte.distribuicaoDfe(config, br.com.swconsultoria.cte.dom.enuns.PessoaEnum.JURIDICA, cnpj, ConsultaDFeEnum.NSU, nsu);
			System.out.println("Status:" + retorno.getCStat());
			System.out.println("Motivo:" + retorno.getXMotivo());
			System.out.println("Max NSU:" + retorno.getMaxNSU());
			System.out.println("Ult NSU:" + retorno.getUltNSU());
			msg = retorno.getCStat() + " - " + retorno.getXMotivo();
			if (StatusCteEnum.DOC_LOCALIZADO_PARA_DESTINATARIO.equals(retorno.getCStat())) {

				List<RetDistDFeInt.LoteDistDFeInt.DocZip> listaDoc = retorno.getLoteDistDFeInt().getDocZip();

				System.out.println("Encontrado " + listaDoc.size() + " Notas.");
				for (RetDistDFeInt.LoteDistDFeInt.DocZip docZip : listaDoc) {
					System.out.println("Schema: " + docZip.getSchema());
					System.out.println("NSU:" + docZip.getNSU());
					System.out.println("XML: " + XmlCteUtil.gZipToXml(docZip.getValue()));
				}
			}

		} catch (CteException | IOException e) {
			System.out.println("Erro:" + e.getMessage());
		} catch (CertificadoException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(msg);
	}

	@RequestMapping(value="/consultar",method= RequestMethod.POST)
	public ResponseEntity<String> consultar(ConfigDTO dto) throws FileNotFoundException {
		String mensagem = "";
		try {

			ConfiguracoesCte config = ConfiguracaoCTe.iniciaConfiguracoes(dto);

			TRetConsStatServ retorno = Cte.statusServico(config);

			mensagem = String.format("Retorno da sefaz: %s - %s.", retorno.getCStat(), retorno.getXMotivo());

			System.out.println("Status:" + retorno.getCStat());
			System.out.println("Motivo:" + retorno.getXMotivo());
			System.out.println("Data:" + retorno.getDhRecbto());

		} catch (CteException | CertificadoException e) {
			System.out.println("Erro:" + e.getMessage());
		}
		return ResponseEntity.ok().body(mensagem);
	}

	@RequestMapping(value="/emitir",method= RequestMethod.POST)
	public ResponseEntity<String> emitir(@RequestBody CteDTO dto) throws FileNotFoundException {
		String mensagem = "";
		try {
			ConfiguracoesCte config = ConfiguracaoCTe.iniciaConfiguracoes(dto.getConfig());

			String cnpj = dto.getCnpj();
			String modelo = dto.getModelo();
			int serie = Integer.parseInt(dto.getSerie());
			int numero = Integer.parseInt(dto.getNumero());;
			String tipoEmissao = dto.getTipoEmissao();
			String cct = String.format("%08d", new Random().nextInt(99999999));

			// Inicia As Configurações

			TCTe cte = new TCTe();
			TCTe.InfCte infCTe = new TCTe.InfCte();

			// Substitua X Pela Chave
			ChaveUtil chaveUtil = new ChaveUtil(config.getEstado(),
					cnpj, modelo, serie, numero,
					tipoEmissao, cct, LocalDateTime.now());

			String chave = chaveUtil.getChaveCT();

			infCTe.setId(chave);
			infCTe.setVersao(ConstantesCte.VERSAO.CTE);

			TCTe.InfCte.Ide ide = new TCTe.InfCte.Ide();
			ide.setCUF(config.getEstado().getCodigoUF());
			ide.setCCT(cct);
			ide.setCFOP(dto.getCfop());
			ide.setNatOp(dto.getNatOp());
			ide.setMod(modelo);
			ide.setSerie(String.valueOf(serie));
			ide.setNCT(String.valueOf(numero));
			ide.setDhEmi(XmlCteUtil.dataCte(LocalDateTime.now()));
			ide.setTpImp(dto.getTpImp());
			ide.setTpEmis(tipoEmissao);
			ide.setCDV(chaveUtil.getDigitoVerificador());
			ide.setTpAmb(config.getAmbiente().getCodigo());
			ide.setTpCTe(dto.getTpCTe());
			//emitido
			ide.setProcEmi("0");
			ide.setVerProc("1.0");
			ide.setCMunEnv(dto.getcMunEnv());
			ide.setXMunEnv(dto.getxMunEnv());
			ide.setUFEnv(TUf.valueOf(dto.getUfEnv()));
			//ide.setModal(dto.getModal());
			ide.setModal("01");
			ide.setTpServ(dto.getTpServ());
			ide.setCMunIni(dto.getcMunIni());
			ide.setXMunIni(dto.getxMunIni());
			ide.setUFIni(TUf.valueOf(dto.getUfIni()));
			ide.setCMunFim(dto.getcMunFim());
			ide.setXMunFim(dto.getxMunFim());
			ide.setUFFim(TUf.valueOf(dto.getUfFim()));
			ide.setRetira(dto.getRetira());
			ide.setIndIEToma(dto.getIndIEToma());

			TCTe.InfCte.Ide.Toma3 toma3 = new TCTe.InfCte.Ide.Toma3();
			toma3.setToma(dto.getToma());
			ide.setToma3(toma3);
			infCTe.setIde(ide);

			TCTe.InfCte.Compl compl = new TCTe.InfCte.Compl();
			compl.setXObs(dto.getxObs());
			infCTe.setCompl(compl);

			TCTe.InfCte.Emit emit = new TCTe.InfCte.Emit();
			emit.setCNPJ(cnpj);
			emit.setIE(dto.getEmitInscricaoEstadual());
			emit.setXNome(dto.getEmitXNome());
			emit.setXFant(dto.getEmitXNome());
			emit.setCRT(dto.getCrt());

			br.com.swconsultoria.cte.schema_400.cte.TEndeEmi enderEmit = new br.com.swconsultoria.cte.schema_400.cte.TEndeEmi();
			enderEmit.setXLgr(dto.getEmitXLgr());
			enderEmit.setNro(dto.getEmitNro());
			enderEmit.setXCpl(dto.getEmitXCpl());
			enderEmit.setXBairro(dto.getEmitBairro());
			enderEmit.setCMun(dto.getEmitCMun());
			enderEmit.setXMun(dto.getEmitXMun());
			enderEmit.setUF(TUFSemEX.valueOf(dto.getEmitUF()));
			enderEmit.setCEP(dto.getEmitCep());
			enderEmit.setFone(dto.getEmitFone());

			emit.setEnderEmit(enderEmit);
			infCTe.setEmit(emit);

			TCTe.InfCte.Rem rem = new TCTe.InfCte.Rem();
			rem.setCNPJ(dto.getRemtCnpj());
			rem.setIE(dto.getRemInscricaoEstadual());
			rem.setXNome(dto.getRemXNome());
			rem.setEmail(dto.getRemEmail());

			br.com.swconsultoria.cte.schema_400.cte.TEndereco enderRem = new br.com.swconsultoria.cte.schema_400.cte.TEndereco();
			enderRem.setXLgr(dto.getRemXLgr());
			enderRem.setNro(dto.getRemNro());
			enderRem.setXBairro(dto.getRemBairro());
			enderRem.setCMun(dto.getRemCMun());
			enderRem.setXMun(dto.getRemXMun());
			enderRem.setUF(TUf.valueOf(dto.getRemUF()));
			enderRem.setCEP(dto.getRemCep());
			enderRem.setCPais(dto.getRemCPais());
			enderRem.setXPais(dto.getRemXPais());
			rem.setEnderReme(enderRem);
			infCTe.setRem(rem);

			TCTe.InfCte.Dest dest = new TCTe.InfCte.Dest();
			dest.setCNPJ(dto.getDestCnpj());
			dest.setIE(dto.getDestCnpjInscricaoEstadual());
			dest.setXNome(dto.getDestXNome());
			dest.setEmail(dto.getDestEmail());

			br.com.swconsultoria.cte.schema_400.cte.TEndereco enderDest = new br.com.swconsultoria.cte.schema_400.cte.TEndereco();
			enderDest.setXLgr(dto.getDestXLgr());
			enderDest.setNro(dto.getDestNro());
			enderDest.setXBairro(dto.getDestEmail());
			enderDest.setCMun(dto.getDestCMun());
			enderDest.setXMun(dto.getDestXMun());
			enderDest.setUF(TUf.valueOf(dto.getDestUF()));
			enderDest.setCEP(dto.getDestCep());
			enderDest.setCPais(dto.getDestCPais());
			enderDest.setXPais(dto.getDestXPais());
			dest.setEnderDest(enderDest);
			infCTe.setDest(dest);

			TCTe.InfCte.VPrest prest = new TCTe.InfCte.VPrest();
			prest.setVTPrest(dto.getvTPrest());
			prest.setVRec(dto.getvRec());

			TCTe.InfCte.VPrest.Comp comp = new TCTe.InfCte.VPrest.Comp();
			comp.setVComp(dto.getvComp());
			comp.setXNome(dto.getPrestXNome());
			prest.getComp().add(comp);
			infCTe.setVPrest(prest);

			TCTe.InfCte.Imp imp = new TCTe.InfCte.Imp();

			br.com.swconsultoria.cte.schema_400.cte.TImp icms = new br.com.swconsultoria.cte.schema_400.cte.TImp();

			br.com.swconsultoria.cte.schema_400.cte.TImp.ICMS45 icms45 = new br.com.swconsultoria.cte.schema_400.cte.TImp.ICMS45();
			icms45.setCST(dto.getCst());
			icms.setICMS45(icms45);

			imp.setICMS(icms);
			infCTe.setImp(imp);

			TCTe.InfCte.InfCTeNorm infCTeNorm = new TCTe.InfCte.InfCTeNorm();

			TCTe.InfCte.InfCTeNorm.InfCarga infCarga = new TCTe.InfCte.InfCTeNorm.InfCarga();
			infCarga.setVCarga(dto.getvCarga());
			infCarga.setProPred(dto.getProPred());

			TCTe.InfCte.InfCTeNorm.InfCarga.InfQ infQ = new TCTe.InfCte.InfCTeNorm.InfCarga.InfQ();
			infQ.setCUnid(dto.getcUnid());
			infQ.setTpMed(dto.getTpMed());
			infQ.setQCarga(dto.getqCarga());
			infCarga.getInfQ().add(infQ);

			TCTe.InfCte.InfCTeNorm.InfDoc infDoc = new TCTe.InfCte.InfCTeNorm.InfDoc();

			TCTe.InfCte.InfCTeNorm.InfDoc.InfNFe infNFe = new TCTe.InfCte.InfCTeNorm.InfDoc.InfNFe();
			infNFe.setChave(dto.getChave());
			infDoc.getInfNFe().add(infNFe);

			TCTe.InfCte.InfCTeNorm.InfModal infModal = new TCTe.InfCte.InfCTeNorm.InfModal();
			infModal.setVersaoModal(ConstantesCte.VERSAO.CTE);

			Rodo rodo = new Rodo();
			//rodo.setRNTRC(dto.getRNTRC());
			rodo.setRNTRC("47008950");

			infModal.setAny(ObjetoCTeUtil.objectToElement(rodo, Rodo.class, "rodo"));

			infCTeNorm.setInfCarga(infCarga);
			infCTeNorm.setInfDoc(infDoc);
			infCTeNorm.setInfModal(infModal);

			infCTe.setInfCTeNorm(infCTeNorm);
			cte.setInfCte(infCTe);
			// MOnta e Assina o XML
			TCTe enviCTe = Cte.montaCte(config, cte, true);

			//Adiciona QRCode
			TCTe.InfCTeSupl infCTeSupl = new TCTe.InfCTeSupl();
			infCTeSupl.setQrCodCTe(ObjetoCTeUtil.criaQRCode(
					cte.getInfCte().getId().substring(3),
					config));

			enviCTe.setInfCTeSupl(infCTeSupl);

			// Envia a Cte para a Sefaz
			TRetCTe retorno = Cte.enviarCte(config, enviCTe);
			Cte.enviarCte(config, enviCTe);
			mensagem = "Status:" + retorno.getCStat() + " - Motivo:" + retorno.getXMotivo() + " - Chave: " + chave;
			if (!retorno.getCStat().equals(StatusCteEnum.AUTORIZADO.getCodigo())) {
				throw new CteException("Status:" + retorno.getCStat() + " - Motivo:" + retorno.getXMotivo());
			}

			System.out.println("Status: " + retorno.getProtCTe().getInfProt().getCStat() + " - " + retorno.getProtCTe().getInfProt().getXMotivo());
			System.out.println("Data: " + retorno.getProtCTe().getInfProt().getDhRecbto());


			System.out.println("Protocolo: " + retorno.getProtCTe().getInfProt().getNProt());
			System.out.println("XML Final: " + XmlCteUtil.criaCteProc(enviCTe, retorno.getProtCTe()));


		} catch (Exception e) {
			System.out.println("Erro:" + e.getMessage());
		}
		return ResponseEntity.ok().body(mensagem);
	}

	@RequestMapping(value = "/cancelar", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> cancelar(@RequestBody CancelamentoCTeDTO dto) throws FileNotFoundException {
		Map<String, Object> mensagem = new HashMap<>();

		try {

			ConfiguracoesCte config = ConfiguracaoCTe.iniciaConfiguracoes(dto.getConfig());

			String chave = dto.getChave();
			String protocolo = dto.getProtocolo();
			String cnpj = config.getCertificado().getCnpjCpf();
			String numeroSeqCancelamento = "001";
			String eventoCancelamento = "110111";
			String id = "ID" + eventoCancelamento + chave + numeroSeqCancelamento;
			String justificativa = dto.getMotivo(); // TODO Preencha

			TEvento enviEvento = new TEvento();
			enviEvento.setVersao(ConstantesCte.VERSAO.CTE);

			TEvento.InfEvento infoEvento = new TEvento.InfEvento();
			infoEvento.setId(id);
			infoEvento.setCOrgao(config.getEstado().getCodigoUF());
			infoEvento.setTpAmb(config.getAmbiente().getCodigo());
			infoEvento.setCNPJ(cnpj);
			infoEvento.setChCTe(chave);
			infoEvento.setDhEvento(XmlCteUtil.dataCte(LocalDateTime.now()));
			infoEvento.setTpEvento(eventoCancelamento);
			infoEvento.setNSeqEvento(Integer.valueOf(numeroSeqCancelamento).toString());

			EvCancCTe eventoCancela = new EvCancCTe();
			eventoCancela.setDescEvento("Cancelamento");
			eventoCancela.setNProt(protocolo);
			eventoCancela.setXJust(justificativa);

			TEvento.InfEvento.DetEvento detEvento = new TEvento.InfEvento.DetEvento();
			detEvento.setVersaoEvento(ConstantesCte.VERSAO.CTE);
			detEvento.setAny(ObjetoCTeUtil.objectToElement(eventoCancela, EvCancCTe.class, "evCancCTe"));

			infoEvento.setDetEvento(detEvento);
			enviEvento.setInfEvento(infoEvento);

			TRetEvento retorno = Cte.cancelarCte(config, enviEvento, true);

			if (!retorno.getInfEvento().getCStat().equals(StatusCteEnum.EVENTO_VINCULADO.getCodigo())) {

				System.out.println("Erro Status:" + retorno.getInfEvento().getCStat());
				System.out.println("Erro Motivo:" + retorno.getInfEvento().getXMotivo());

				mensagem.put("status", retorno.getInfEvento().getCStat());
				mensagem.put("motivo", retorno.getInfEvento().getXMotivo());
				mensagem.put("chave", chave);
			} else {

				System.out.println("Status:" + retorno.getInfEvento().getCStat());
				System.out.println("Motivo:" + retorno.getInfEvento().getXMotivo());
				System.out.println("Data:" + retorno.getInfEvento().getDhRegEvento());

				// Cria TProcEventoCTe
				TProcEvento procEvento = new TProcEvento();
				procEvento.setVersao(ConstantesCte.VERSAO.CTE);
				procEvento.setEventoCTe(enviEvento);
				procEvento.setRetEventoCTe(retorno);

				System.out.println("Xml Final Cancelamento Proc: " + XmlCteUtil.objectToXml(procEvento));

				mensagem.put("status", retorno.getInfEvento().getCStat());
				mensagem.put("motivo", retorno.getInfEvento().getXMotivo());
				mensagem.put("chave", chave);
			}

		} catch (CteException | JAXBException e) {
			mensagem.put("motivo", e.getMessage());
			System.out.println("Erro:" + e.getMessage());
			e.printStackTrace();
		} catch (CertificadoException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok().body(mensagem);
	}
}
