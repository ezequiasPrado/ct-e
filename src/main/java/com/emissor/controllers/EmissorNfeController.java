package com.emissor.controllers;

import br.com.swconsultoria.certificado.exception.CertificadoException;
import br.com.swconsultoria.nfe.dom.ConfiguracoesNfe;
import br.com.swconsultoria.nfe.dom.enuns.StatusEnum;
import br.com.swconsultoria.nfe.exception.NfeException;
import br.com.swconsultoria.nfe.schema_4.enviNFe.*;
import br.com.swconsultoria.nfe.schema_4.retConsReciNFe.TRetConsReciNFe;
import br.com.swconsultoria.nfe.util.ChaveUtil;
import br.com.swconsultoria.nfe.util.ConstantesUtil;
import br.com.swconsultoria.nfe.util.RetornoUtil;
import br.com.swconsultoria.nfe.util.XmlNfeUtil;
import com.emissor.config.ConfiguracaoNFe;
import com.emissor.dto.CancelamentoNFeDTO;
import com.emissor.dto.ConfigDTO;
import com.emissor.dto.NFeDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.swconsultoria.nfe.Nfe;
import br.com.swconsultoria.nfe.dom.enuns.DocumentoEnum;
import br.com.swconsultoria.nfe.schema_4.retConsStatServ.TRetConsStatServ;

import br.com.swconsultoria.nfe.dom.Evento;
import br.com.swconsultoria.nfe.schema.envEventoCancNFe.TEnvEvento;
import br.com.swconsultoria.nfe.schema.envEventoCancNFe.TRetEnvEvento;
import br.com.swconsultoria.nfe.util.CancelamentoUtil;



import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/api/nfe")
public class EmissorNfeController {

	@RequestMapping(value = "/consultar", method = RequestMethod.GET)
	public ResponseEntity<String> consultar() throws FileNotFoundException {
		String mensagem = "";
		try {

			ConfiguracoesNfe config = ConfiguracaoNFe.iniciaConfiguracoes(new ConfigDTO());

			//Efetua Consulta
			TRetConsStatServ retorno = Nfe.statusServico(config, DocumentoEnum.NFE);

			mensagem = String.format("Retorno da sefaz: %s - %s.", retorno.getCStat(), retorno.getXMotivo());

			System.out.println("Status:" + retorno.getCStat());
			System.out.println("Motivo:" + retorno.getXMotivo());
			System.out.println("Data:" + retorno.getDhRecbto());

		} catch (CertificadoException | NfeException e) {
			System.out.println("Erro:" + e.getMessage());
		}
		return ResponseEntity.ok().body(mensagem);
	}

	@RequestMapping(value = "/emitir", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> emitir(@RequestBody NFeDTO dto) throws FileNotFoundException {
		Map<String, Object> mensagem = new HashMap<>();
		try {
			// Inicia As Configurações - ver https://github.com/Samuel-Oliveira/Java_NFe/wiki/1-:-Configuracoes
			ConfiguracoesNfe config = ConfiguracaoNFe.iniciaConfiguracoes(dto.getConfig());

			//Informe o Numero da NFe
			int numeroNfe = dto.getNumeroNfe();
			//Informe o CNPJ do Emitente da NFe
			String cnpj = dto.getCnpj();
			//Informe a data de Emissao da NFe
			LocalDateTime dataEmissao = LocalDateTime.now();
			//Informe o cnf da NFCe com 8 digitos
			Random n = new Random();
			String cnf = ChaveUtil.completarComZerosAEsquerda(String.valueOf(numeroNfe * 2 + n.nextInt(100)), 8);
			//Informe o modelo da NFe
			String modelo = DocumentoEnum.NFE.getModelo();
			//Informe a Serie da NFe
			int serie = dto.getSerie();
			//Informe o tipo de Emissao da NFe
			String tipoEmissao = dto.getTipoEmissao();

			// MontaChave a NFe
			ChaveUtil chaveUtil = new ChaveUtil(config.getEstado(), cnpj, modelo, serie, numeroNfe, tipoEmissao, cnf, dataEmissao);
			String chave = chaveUtil.getChaveNF();
			String cdv = chaveUtil.getDigitoVerificador();

			mensagem.put("chave", chave);
			mensagem.put("numero", numeroNfe);
			TNFe.InfNFe infNFe = new TNFe.InfNFe();
			infNFe.setId(chave);
			infNFe.setVersao(ConstantesUtil.VERSAO.NFE);

			//Preenche IDE
			infNFe.setIde(preencheIde(config, cnf, numeroNfe, tipoEmissao, modelo, serie, cdv, dataEmissao, dto));

			//Preenche Emitente
			infNFe.setEmit(preencheEmitente(config, cnpj, dto));

			//Preenche o Destinatario
			infNFe.setDest(preencheDestinatario(dto));

			//Preenche os dados do Produto da Nfe e adiciona a Lista
			infNFe.getDet().addAll(preencheDet());

			//Preenche totais da NFe
			infNFe.setTotal(preencheTotal());

			//Preenche os dados de Transporte
			infNFe.setTransp(preencheTransporte());

			// Preenche dados Pagamento
			infNFe.setPag(preenchePag());

			TNFe nfe = new TNFe();
			nfe.setInfNFe(infNFe);

			TInfRespTec respTec = new TInfRespTec();
			respTec.setCNPJ("13490226000160");
			respTec.setEmail("responsavel@gmail.com");
			respTec.setFone("3332412047");
			respTec.setXContato("João Luiz");
			infNFe.setInfRespTec(respTec);

			// Monta EnviNfe
			TEnviNFe enviNFe = new TEnviNFe();
			enviNFe.setVersao(ConstantesUtil.VERSAO.NFE);
			enviNFe.setIdLote("1");
			enviNFe.setIndSinc("1");
			enviNFe.getNFe().add(nfe);

			// Monta e Assina o XML
			enviNFe = Nfe.montaNfe(config, enviNFe, true);

			// Envia a Nfe para a Sefaz
			TRetEnviNFe retorno = Nfe.enviarNfe(config, enviNFe, DocumentoEnum.NFE);

			//Valida se o Retorno é Assincrono
			if (RetornoUtil.isRetornoAssincrono(retorno)) {
				//Pega o Recibo
				String recibo = retorno.getInfRec().getNRec();
				int tentativa = 0;
				TRetConsReciNFe retornoNfe = null;

				//Define Numero de tentativas que irá tentar a Consulta
				while (tentativa < 15) {
					retornoNfe = Nfe.consultaRecibo(config, recibo, DocumentoEnum.NFE);
					if (retornoNfe.getCStat().equals(StatusEnum.LOTE_EM_PROCESSAMENTO.getCodigo())) {
						System.out.println("INFO: Lote Em Processamento, vai tentar novamente apos 1 Segundo.");
						Thread.sleep(1000);
						tentativa++;
					} else {
						break;
					}
				}

				RetornoUtil.validaAssincrono(retornoNfe);
				System.out.println();
				System.out.println("# Status: " + retornoNfe.getProtNFe().get(0).getInfProt().getCStat() + " - " + retornoNfe.getProtNFe().get(0).getInfProt().getXMotivo());
				System.out.println("# Protocolo: " + retornoNfe.getProtNFe().get(0).getInfProt().getNProt());
				System.out.println("# XML Final: " + XmlNfeUtil.criaNfeProc(enviNFe, retornoNfe.getProtNFe().get(0)));

				mensagem.put("status", retorno.getProtNFe().getInfProt().getCStat());
				mensagem.put("motivo", retorno.getProtNFe().getInfProt().getXMotivo());
				mensagem.put("protocolo", retorno.getProtNFe().getInfProt().getNProt());
				mensagem.put("xml", XmlNfeUtil.criaNfeProc(enviNFe, retornoNfe.getProtNFe().get(0)));
			} else {
				//Se for else o Retorno é Sincrono

				//Valida Retorno Sincrono
				RetornoUtil.validaSincrono(retorno);
				System.out.println();
				System.out.println("# Status: " + retorno.getProtNFe().getInfProt().getCStat() + " - " + retorno.getProtNFe().getInfProt().getXMotivo());
				System.out.println("# Protocolo: " + retorno.getProtNFe().getInfProt().getNProt());
				System.out.println("# Xml Final :" + XmlNfeUtil.criaNfeProc(enviNFe, retorno.getProtNFe()));

				mensagem.put("status", retorno.getProtNFe().getInfProt().getCStat());
				mensagem.put("motivo", retorno.getProtNFe().getInfProt().getXMotivo());
				mensagem.put("protocolo", retorno.getProtNFe().getInfProt().getNProt());
				mensagem.put("xml", XmlNfeUtil.criaNfeProc(enviNFe, retorno.getProtNFe()));
			}

		} catch (Exception e) {
			mensagem.put("motivo", e.getMessage());
			System.err.println();
			System.err.println("# Erro: " + e.getMessage());
		}
		return ResponseEntity.ok().body(mensagem);
	}

	/**
	 * Preenche o IDE
	 * @param config
	 * @param cnf
	 * @param numeroNfe
	 * @param tipoEmissao
	 * @param cDv
	 * @param dataEmissao
	 * @return
	 * @throws NfeException
	 */
	private static TNFe.InfNFe.Ide preencheIde(ConfiguracoesNfe config, String cnf, int numeroNfe, String tipoEmissao, String modelo, int serie, String cDv, LocalDateTime dataEmissao, NFeDTO dto) throws NfeException {
		TNFe.InfNFe.Ide ide = new TNFe.InfNFe.Ide();
		ide.setCUF(config.getEstado().getCodigoUF());
		ide.setCNF(cnf);
		ide.setNatOp(dto.getNatOp());
		ide.setMod(modelo);
		ide.setSerie(String.valueOf(serie));

		ide.setNNF(String.valueOf(numeroNfe));
		ide.setDhEmi(XmlNfeUtil.dataNfe(dataEmissao));
		ide.setTpNF(dto.getTpNF());
		ide.setIdDest(dto.getIdDest());
		ide.setCMunFG(dto.getCodMunFG());
		ide.setTpImp(dto.getTpImp());
		ide.setTpEmis(tipoEmissao);
		ide.setCDV(cDv);
		ide.setTpAmb(config.getAmbiente().getCodigo());
		ide.setFinNFe(dto.getFinNFe());
		ide.setIndFinal(dto.getIndFinal());
		ide.setIndPres(dto.getIndPres());
		ide.setProcEmi(dto.getProcEmi());
		ide.setVerProc("1.0");

		return ide;
	}

	/**
	 * Preenche o Emitente da Nfe
	 * @param config
	 * @param cnpj
	 * @return
	 */
	private static TNFe.InfNFe.Emit preencheEmitente(ConfiguracoesNfe config, String cnpj, NFeDTO dto) {
		TNFe.InfNFe.Emit emit = new TNFe.InfNFe.Emit();
		emit.setCNPJ(cnpj);
		emit.setXNome(dto.getEmitXNome());

		TEnderEmi enderEmit = new TEnderEmi();
		enderEmit.setXLgr(dto.getEmitXLgr());
		enderEmit.setNro(dto.getEmitNro());
		enderEmit.setXCpl(dto.getEmitXCpl());
		enderEmit.setXBairro(dto.getEmitBairro());
		enderEmit.setCMun(dto.getEmitCMun());
		enderEmit.setXMun(dto.getEmitXMun());
		enderEmit.setUF(TUfEmi.valueOf(config.getEstado().toString()));
		enderEmit.setCEP(dto.getEmitCep());
		enderEmit.setFone(dto.getEmitFone());
		enderEmit.setCPais("1058");
		enderEmit.setXPais("Brasil");
		emit.setEnderEmit(enderEmit);

		emit.setIE(dto.getEmitInscricaoEstadual());
		emit.setCRT(dto.getCrt());

		return emit;
	}

	/**
	 * Preenche o Destinatario da NFe
	 * @return
	 */
	private static TNFe.InfNFe.Dest preencheDestinatario(NFeDTO dto) {
		TNFe.InfNFe.Dest dest = new TNFe.InfNFe.Dest();
		dest.setCNPJ(dto.getDestCnpj());
		dest.setIndIEDest(dto.getIndIEDest());
		dest.setXNome(dto.getDestXNome());
		dest.setEmail(dto.getDestEmail());

		TEndereco enderDest = new TEndereco();
		enderDest.setXLgr(dto.getDestXLgr());
		enderDest.setNro(dto.getDestNro());
		enderDest.setXBairro(dto.getDestEmail());
		enderDest.setCMun(dto.getDestCMun());
		enderDest.setXMun(dto.getDestXMun());
		enderDest.setUF(TUf.valueOf(dto.getDestUF()));
		enderDest.setCEP(dto.getDestCep());
		enderDest.setCPais(dto.getDestCPais());
		enderDest.setXPais(dto.getDestXPais());
		enderDest.setFone(dto.getDestFone());
		dest.setEnderDest(enderDest);

		return dest;
	}

	/**
	 * Preenche Det Nfe
	 */
	private static List<TNFe.InfNFe.Det> preencheDet() {

		//O Preenchimento deve ser feito por produto, Então deve ocorrer uma LIsta
		TNFe.InfNFe.Det det = new TNFe.InfNFe.Det();
		//O numero do Item deve seguir uma sequencia
		det.setNItem("1");

		// Preenche dados do Produto
		det.setProd(preencheProduto());

		//Preenche dados do Imposto
		det.setImposto(preencheImposto());

		//Retorna a Lista de Det
		return Collections.singletonList(det);
	}

	/**
	 * Preenche dados do Produto
	 * @return
	 */
	private static TNFe.InfNFe.Det.Prod preencheProduto() {
		TNFe.InfNFe.Det.Prod prod = new TNFe.InfNFe.Det.Prod();
		prod.setCProd("7898480650104");
		prod.setCEAN("7898480650104");
		prod.setXProd("NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL");
		prod.setNCM("27101932");
		prod.setCEST("0600500");
		prod.setIndEscala("S");
		prod.setCFOP("5405");
		prod.setUCom("UN");
		prod.setQCom("1.0000");
		prod.setVUnCom("13.0000");
		prod.setVProd("13.00");
		prod.setCEANTrib("7898480650104");
		prod.setUTrib("UN");
		prod.setQTrib("1.0000");
		prod.setVUnTrib("13.0000");
		prod.setIndTot("1");

		return prod;
	}

	/**
	 * Preenche dados do Imposto da Nfe
	 * @return
	 */
	private static TNFe.InfNFe.Det.Imposto preencheImposto() {
		TNFe.InfNFe.Det.Imposto imposto = new TNFe.InfNFe.Det.Imposto();

		TNFe.InfNFe.Det.Imposto.ICMS icms = new TNFe.InfNFe.Det.Imposto.ICMS();

		TNFe.InfNFe.Det.Imposto.ICMS.ICMS00 icms00 = new TNFe.InfNFe.Det.Imposto.ICMS.ICMS00();
		icms00.setOrig("0");
		icms00.setCST("00");
		icms00.setModBC("0");
		icms00.setVBC("13.00");
		icms00.setPICMS("7.00");
		icms00.setVICMS("0.91");

		icms.setICMS00(icms00);

		TNFe.InfNFe.Det.Imposto.PIS pis = new TNFe.InfNFe.Det.Imposto.PIS();
		TNFe.InfNFe.Det.Imposto.PIS.PISAliq pisAliq = new TNFe.InfNFe.Det.Imposto.PIS.PISAliq();
		pisAliq.setCST("01");
		pisAliq.setVBC("13.00");
		pisAliq.setPPIS("1.65");
		pisAliq.setVPIS("0.21");
		pis.setPISAliq(pisAliq);

		TNFe.InfNFe.Det.Imposto.COFINS cofins = new TNFe.InfNFe.Det.Imposto.COFINS();
		TNFe.InfNFe.Det.Imposto.COFINS.COFINSAliq cofinsAliq = new TNFe.InfNFe.Det.Imposto.COFINS.COFINSAliq();
		cofinsAliq.setCST("01");
		cofinsAliq.setVBC("13.00");
		cofinsAliq.setPCOFINS("7.60");
		cofinsAliq.setVCOFINS("0.99");
		cofins.setCOFINSAliq(cofinsAliq);

		imposto.getContent().add(new ObjectFactory().createTNFeInfNFeDetImpostoICMS(icms));
		imposto.getContent().add(new ObjectFactory().createTNFeInfNFeDetImpostoPIS(pis));
		imposto.getContent().add(new ObjectFactory().createTNFeInfNFeDetImpostoCOFINS(cofins));

		return imposto;
	}

	/**
	 * Prenche Total NFe
	 * @return
	 */
	private static TNFe.InfNFe.Total preencheTotal() {
		TNFe.InfNFe.Total total = new TNFe.InfNFe.Total();
		TNFe.InfNFe.Total.ICMSTot icmstot = new TNFe.InfNFe.Total.ICMSTot();
		icmstot.setVBC("13.00");
		icmstot.setVICMS("0.91");
		icmstot.setVICMSDeson("0.00");
		icmstot.setVFCP("0.00");
		icmstot.setVFCPST("0.00");
		icmstot.setVFCPSTRet("0.00");
		icmstot.setVBCST("0.00");
		icmstot.setVST("0.00");
		icmstot.setVProd("13.00");
		icmstot.setVFrete("0.00");
		icmstot.setVSeg("0.00");
		icmstot.setVDesc("0.00");
		icmstot.setVII("0.00");
		icmstot.setVIPI("0.00");
		icmstot.setVIPIDevol("0.00");
		icmstot.setVPIS("0.21");
		icmstot.setVCOFINS("0.99");
		icmstot.setVOutro("0.00");
		icmstot.setVNF("13.00");
		total.setICMSTot(icmstot);

		return total;
	}

	/**
	 * Preenche Transporte
	 * @return
	 */
	private static TNFe.InfNFe.Transp preencheTransporte(){
		TNFe.InfNFe.Transp transp = new TNFe.InfNFe.Transp();
		transp.setModFrete("9");
		return transp;
	}

	/**
	 * Preenche dados Pagamento
	 * @return
	 */
	private static TNFe.InfNFe.Pag preenchePag() {
		TNFe.InfNFe.Pag pag = new TNFe.InfNFe.Pag();
		TNFe.InfNFe.Pag.DetPag detPag = new TNFe.InfNFe.Pag.DetPag();
		detPag.setTPag("01");
		detPag.setVPag("13.00");
		pag.getDetPag().add(detPag);

		return pag;
	}

	@RequestMapping(value = "/cancelar", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> cancelar(@RequestBody CancelamentoNFeDTO dto) throws FileNotFoundException {
		Map<String, Object> mensagem = new HashMap<>();

		try {
			ConfiguracoesNfe config =  ConfiguracaoNFe.iniciaConfiguracoes(dto.getConfig());

			//Agora o evento pode aceitar uma lista de cancelaemntos para envio em Lote.
			//Para isso Foi criado o Objeto Cancela
			Evento cancela = new Evento();
			//Informe a chave da Nota a ser Cancelada
			cancela.setChave(dto.getChave());
			//Informe o protocolo da Nota a ser Cancelada
			cancela.setProtocolo(dto.getProtocolo());
			//Informe o CNPJ do emitente
			cancela.setCnpj(config.getCertificado().getCnpjCpf());
			//Informe o Motivo do Cancelamento
			cancela.setMotivo(dto.getMotivo());
			//Informe a data do Cancelamento
			cancela.setDataEvento(LocalDateTime.now());

			//Monta o Evento de Cancelamento
			TEnvEvento enviEvento = CancelamentoUtil.montaCancelamento(cancela, config);

			//Envia o Evento de Cancelamento
			TRetEnvEvento retorno = Nfe.cancelarNfe(config, enviEvento, true, DocumentoEnum.NFE);

			//Valida o Retorno do Cancelamento
			RetornoUtil.validaCancelamento(retorno);

			//Resultado
			System.out.println();
			retorno.getRetEvento().forEach( resultado -> {
				System.out.println("# Chave: " + resultado.getInfEvento().getChNFe());
				System.out.println("# Status: " + resultado.getInfEvento().getCStat() + " - " + resultado.getInfEvento().getXMotivo());
				System.out.println("# Protocolo: " + resultado.getInfEvento().getNProt());

				mensagem.put("status", resultado.getInfEvento().getCStat());
				mensagem.put("motivo", resultado.getInfEvento().getXMotivo());
				mensagem.put("chave", resultado.getInfEvento().getChNFe());
				mensagem.put("protocolo", resultado.getInfEvento().getNProt());
			});

			//Cria ProcEvento de Cacnelamento
			String proc = CancelamentoUtil.criaProcEventoCancelamento(config, enviEvento, retorno.getRetEvento().get(0));
			System.out.println();
			System.out.println("# ProcEvento : " + proc);

		} catch (Exception e) {
			mensagem.put("motivo", "# Erro: "+ e.getMessage());
			System.err.println();
			System.err.println("# Erro: "+e.getMessage());
		}
		return ResponseEntity.ok().body(mensagem);
	}
}
