package com.emissor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class EmissorApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(EmissorApplication.class, args);
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedOrigins("*")
				.allowedMethods("HEAD,GET,POST,PUT,DELETE,PATCH,OPTIONS".split(","))
				.allowedHeaders(("Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,"
						+ "Access-Control-Request-Headers,App-Context,App-Links,Authorization,"
						+ "User-Access,Filter-Encoded").split(","));
	}

}
